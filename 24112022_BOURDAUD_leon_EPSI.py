import pandas as pd
import numpy as np

print("La version pandas installée est :", pd.__version__)

print("""

""")

myDataset  = pd.DataFrame({'col1':[1,2,3],'col2':[1.0,2.0,3.0],'col3':["a","b","c"]})
print(myDataset, "\n")
print("le type des colonnes : \n", myDataset.dtypes, "\n")

print("""

Avec Numpy : 

""")

npArray = np.array([[1,2,3],[4,5,6],[7,8,9]])
myDataset = pd.DataFrame(npArray, columns=['col1','col2','col3'])

print("premier affichage de myDataset")
print(myDataset, "\n")

npArray[0,0] = 99

print("Deuxième affichage de myDataset")
print(myDataset, "\n")

print("""

Avec un fichier exterieur : 

""")

myDataset = pd.read_csv('cours-python\Datasets\ozone.csv')
print(myDataset, "\n")

myDataset = pd.read_csv('cours-python\Datasets\ozone.txt', delimiter = "\t") #pour afficher un dossier txt
print(myDataset, "\n")

print("""

accès aux données d'un DataFrame : 

""")

#myDataset = pd.read_csv('cours-python\Datasets\ozone.csv')
#for index, row in myDataset.iterrows() :
#    print("La valeur d'indice [{}] : \n{}".format(index,row))

#on met en commentaire parce que ça prend trop de place

print("""

On met un filtre : 

""")

myDataset = pd.read_csv('cours-python\Datasets\ozone.csv')
dayOff = myDataset.loc[myDataset["JOUR"]==1]
print(dayOff.head(5))

print("""

Exo : 

""")

myDataset = pd.read_csv('cours-python\Datasets\ozone.csv')
jourFeriee = myDataset.loc[myDataset["O3obs"]>=100]
print(jourFeriee.head(5))

print("""

problème : 

""")

myDataset = pd.read_csv('cours-python\Datasets\ozone.csv')
print("Les valeurs uniques du champ STATION de myDataset : ",myDataset['STATION'].unique())

print("""

Sélectionner des colonnes: 

""")

myDataset = pd.read_csv('cours-python\Datasets\ozone.csv')
VentMOD = myDataset["VentMOD"]
print(VentMOD.head(5),"\n")

myDataset = pd.read_csv('cours-python\Datasets\ozone.csv')
VentMOD = myDataset[["VentMOD", "JOUR"]]
print(VentMOD[0:5])

print("""

accéder à une valeur précise : 

""")

myDataset = pd.read_csv('cours-python\Datasets\ozone.csv')
print(myDataset.iloc[8,2])
