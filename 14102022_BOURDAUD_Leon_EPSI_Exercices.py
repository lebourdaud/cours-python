print("""
Exercice variables etc
""")

            #Exo 1

prenom = "Pierre"
age = 20
majeur = True
compte_en_banque = 20135.384
amis = ["Marie", "Julien", "Adrien"]
parents = ("Marc", "Caroline")

print("je vous présente", prenom, age, "ans", majeur, "avec", compte_en_banque, "euros", "ses amis :", amis, "ses parents :", parents)

print("") #Fin

            #Exo 2

site_web = "google" # " au lieu de ''
print(site_web)

print("") #Fin

            #Exo 3

nombre = 17

print("le nombre est", nombre)

print("") #Fin

            #Exo 4

a = 3
b = 6
a = b
b = 7

# on se retrouve avec a = 6
print("a :", a)

print("") #Fin

            #Exo 5

a = 2
b = 6
c = 3

print(a,b,c, sep=" + ")


print("") #Fin

            #Exo 6

list1 = range(3) #faut pas l'appeller liste sinon il comprend pas le pauvre (parce que list c'est déjà une fonction utilisée par python)
list2 = range(5)

print(list(list2))

print("") #Fin

            #Exo 7

def verifTypeBool(typeTest) :
    return type(typeTest)

prenom = "Vincent"
prenom2 = 0

print(verifTypeBool(prenom) == str, verifTypeBool(prenom))
print(verifTypeBool(prenom2) == str, verifTypeBool(prenom2))


def verifTypeText(typeTest) :
    text = "cette variable est un(e) :" + str(type(typeTest))
    return text

print(verifTypeText(prenom))
print(verifTypeText(prenom2))

print("") #Fin

            #Exo 8
#remplacer un mot par un autre dans la chaine de charactère

chaine ="Salut les devs" #remplacer Salut par Bonsoir

chaine = chaine.replace("Salut", "Bonsoir")

print(chaine)

#plus difficile :

chaine ="Salut les devs. Salut les copains" #remplacer le premier Salut par Bonsoir

chaine = chaine.replace("Salut", "Bonsoir", 1)

print(chaine)
#ha bah ct pas si difficile

print("") #Fin

            #Exo 9



print("") #Fin

            #Exo 10



print("") #Fin

            #Exo 11



print("") #Fin



