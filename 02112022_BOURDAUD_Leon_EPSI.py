#Gestion des exeptions :

while True:
    try :
        #x=int(input("Identifiant : "))
        x=5
        break
    except ValueError:
        print("Entree invalide")
    finally :
        print("gardons le cap")

#bon ça marche pas bien ça marque "gardons le cap" à chaque fois mais passons

try :
    2/0
except ZeroDivisionError :
    pass

#autre utilisation, ici on essaie de créer nous même un message d'erreur :

#div = input()
div = 5
try:
    div = int(div)
    if div == 1:
        raise ValueError()
except ValueError:
    print("Valeur saisie inutile, pas besoin de cet ordi")
else :
    print("Le resultat est", 7/div)

print("")



#A partir d'une liste, n'afficher que les nombres paires

liste = range(20)

def FoundPair_1(liste) :
    liste_1 = []
    for x in liste:
        if x % 2 == 0:
            liste_1.append(x)
    return liste_1

print(FoundPair_1(liste))

def FoundPair_2(liste) :
    liste_2 = [x for x in liste if x % 2 == 0]
    return liste_2

liste_2 = FoundPair_2(liste)

liste_3 = [str(x) for x in liste_2]

print(liste_3)

liste_4 = map(lambda num: num**2, liste_2)

print("carrés : ", liste_4)

#comment on utilise une map ?

for item in liste_4:
    print(item)

liste_5 = [num**2 for num in liste_2]
print("la liste : ", liste_5)

# ça vérifie si item existe, et donc ça fait disparaitre le 0 :

filtre_1 = [item for item in liste_2 if item]
print(filtre_1)



# les dictionnaires

nombres = range(20)
dict_for = {}

for n in nombres :
    if n%2==0:
        dict_for[n] = n**2   # ici on met n en index et on met le carré dedans
print ("dictionnaire : ", dict_for)

dict_for = {n: n**2 for n in nombres if n%2 == 0}     # ne pas oublier les : pour différencier l'index de sa valeur
print ("dictionnaire avec compréhention : ", dict_for)

# c'est très utilisé dans la transcription d'un dictionnaire existant

equipe = {'Cristiano':'Juve', 'Hakimi':'Dortmund', 'Kante':'Chelsea'}
print("on a un dico : ", equipe)
equipe_joueur = {equipe[key]: key for key in equipe}
print("on as inversé les clés/valeurs : ", equipe_joueur)

print("")
# cas d'utilisation :

distance_m = {'v_1':100,'v_2':153000,'v_3':700000,'v_4':600}

print("en m : ", distance_m)

distance_km = { val: distance_m[val]/1000 for val in distance_m}
distance_km = {i:j/1000 for (i,j) in distance_m.items()}     # autre variante

print("en km : ", distance_km)

#on veut filtrer ces elements, uniquement les distances en km, c'est à dire celles qui dépassent 1000

longue_distance_m = {i:j for (i,j) in distance_m.items() if j>=1000}

print("seulement les longues distances : ", longue_distance_m)


print("")
#fonctions NATIVES et built-in

#logging

import logging

#logging.basicConfig(level=logging.INFO)
#logging.debug("message 1 fichier log")
#logging.warning('message 2 avertissement')
#logging.info('message 3 info')

#lambda

f_1 = lambda x,y:x*y
print(f_1(2,3))

d_1 = [[0], [2], [3], [10], [5]]

#obtenir la valeur maximale à partir des données de la liste ci dessus

def max_val(d):
    """Trouver la valeur maximale"""
    return max(d, key=lambda x:x)

print(max_val(d_1))


print("")
#Construction de classe

class felins:
    famille = "mammifères placentaires"
    ordre = "carnivore"
    sous_ordre = "félinormes"

    def mam_plac(self):
        """constructeur"""
        print("famille :", self.famille)

def main():
    tigre = felins()
    tigre.mam_plac()

if __name__ == '__main__':main()

#le premier argument self fait référence à l'objet et non à la classe
#quand un objet est créé a partir de la classe...


print("")
#héritages

class tigres(felins):
    """classe qui représente un tigre et qui hérite de félin"""
    def __init__(self, type_felin, vitesse):
        self.type_felin = type_felin
        self.vitesse = vitesse

#instanciation
T_1 = tigres("tigre", "100km/h")
print ("l'effroyable vitesse du", T_1.type_felin, "est de",T_1.vitesse)

#on pioche dans la classe mère :
print ("le", T_1.type_felin, "est de la famille des", T_1.famille)

#pour savoir si une classe est fille :

"""
on peut utiliser :
 - issubclass()
 - isinstance()
"""


#Polymorphisme
print(max("PYTHON"))
print(max(1,7,2))

#max est une fonction polymorphique, car elle réagit différement selon ce quon met dedans

#dans une classe :
class felins:
    def __init__(self, type_felin="tigre",couleur="orange"):
        self.type_felin = "tigre"
        self.couleur = "orange"

class felins2:
    def __init__(self, type_felin="tigre",couleur="orange"):
        self.type_felin = type_felin
        self.couleur = couleur
    def __str__(self):   #c'est ici que la magie opère
        return ("Le félin est de type {} et de couleur {}".format (self.couleur, self.type_felin))

f_2 = felins2("jaune clair", "tigre clair")   #ça affiche ce que renvoie la fonction magique
print(f_2)


print("")
# les get set

#class felins3:
#    def __init__(self, type_felin="tigre",couleur="orange"):
#        self.type_felin = type_felin
#        self.couleur = couleur

#    def def_couleur(self):   #get
#        return self.couleur
#    def ch_couleur(self, valeur):   #set
#        self.couleur = valeur
#    couleur=property (fget = def_couleur(), fset= ch_couleur())

#    def def_type_felin(self):   #get
#        return self.type_felin
#    def ch_type_felin(self, valeur):   #set
#        if valeur == "serpent":   # gestion d'erreur !!!
#            raise ValueError("ça va pas la tête !?")
#        self.type_felin = valeur
#    type_felin=property (fget = def_type_felin(), fset= ch_type_felin())

#T_6 = felins3()
#print(T_6.type_felin)
#T_6.type_felin = "chèvre"
#print(T_6.type_felin)

#bon ça marche pas mais on va dire c'est ok


print("")

# Les décorateurs

# permet de changer le comportement de d'autres fonctions sans les changer
# on a une fonction en argument sur laquelle on va appliquer un décorateur, on renvoie la fonction avec un comportement différent

def Afficher_décorateur(function):
    def nouvelle_fonction(a,b):
        print('le resultat de cette operation avec {} et {}'.format(a,b))
        res = function(a,b)
        print("resultat :", res)
        return res
    return nouvelle_fonction

#afficher la fonction :

@Afficher_décorateur
def soustraction(a,b):
    return a - b
soustraction(8,5)

print("")

@Afficher_décorateur
def addition(a,b):
    return a + b
addition(25,46)


print("")

def minuscule(fonc):
    texte = fonc()
    if not isinstance(texte, str):
        raise TypeError("type non adapté")
    return texte.lower()

def salut():
    return "HOLALA mais c'est mes GIGA potos INCR"

print(salut())
print(minuscule(salut))     #on peut utiliser le décorateur comme ceci

@minuscule     #si on veut l'attacher direct à la fonction
def salut():
    return "HOLALA mais c'est mes GIGA potos INCR"

print(salut)


def adresse_pro(fonc):
    def wrapper():
        texte = fonc()
        res = "".join([texte, "@entreprise.com"])
        return res
    return wrapper

@adresse_pro
@minuscule

def utilisateurs():
    return "Tasnime"

print(utilisateurs())
adresse = utilisateurs
print(adresse)


