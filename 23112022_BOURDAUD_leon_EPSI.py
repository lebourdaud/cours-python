#data science / machine learning

# on veut apprendre à utiliser numpy

import numpy as np

vecteur = np.array([1,2,3,4])
print("vecteur = ", vecteur)

vecteur_2d = np.array([[1,2,3,4], [1,2,3,4]])
print("vecteur 2d = \n", vecteur_2d)

vecteur_3d = np.array([[[1,2,3,4], [1,2,3,4], [1,2,3,4]],[[1,2,3,4], [1,2,3,4], [1,2,3,4]]])
print("vecteur 3d = \n", vecteur_3d)
print("vecteur 3d shape = ", vecteur_3d.shape) #shape donne la dimention du tableau (ici 2 matrices, 3 lignes, 4 colonnes)
print("vecteur 3d ndim = ", vecteur_3d.ndim) #ndim donne le nombre de dimentions

print("""

Initialisation : 

""")

#initialisation

vecteur_1 = np.zeros(10) #c'est des float de base
vecteur_2 = np.zeros(10, dtype='int32') #ici on fait en sorte que ce soit de l'int
vecteur_3 = np.ones((3,4), dtype='int16') # et là on génère un tableau à 2 dimentions

print("vecteur 1 = ", vecteur_1)
print("vecteur 2 = ", vecteur_2)
print("vecteur 3 = \n", vecteur_3)

print("""

Avec tableau en paramètres : 

""")

vecteur_1 = np.full((3,4), 5) # full remplie le tableau avec une certaine donnée (ici 5)
vecteur_2 = np.full_like(vecteur_1, 10) # on récupère les paramètres du vecteur 1 (en l'occurence la taille du tableau)
vecteur_3 = np.identity(3) #ça fait un 1 en diagonale...

print("vecteur 1 = \n", vecteur_1)
print("vecteur 2 = \n", vecteur_2)
print("vecteur 3 = \n", vecteur_3)

