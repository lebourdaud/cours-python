# c'est pour l'exercice :
import random

x = tuple(("pomme", "banana", "cerise"))
x = 3 + 5j
print(type(x))

x = float(1)
y = int(2.8)
z = complex(1)

print(x)
print(y)
print(z)

a = float(x)
b = int(y)
c = complex(x)

print(a, b, c)
print(type(a), type(b), type(c))

#Exercices !!!
#afficher un nombre aléaoire entre 1 et 10
random = 1
random2 = 2


while random == random2 :
    random = random.randrange(1,11)#11 exclu
    random2 = random.randint(1,10)#10 inclu
    print(random,random2)
#ça marche pas parce que il me dit que randrange c'est pas un int, on verra plus tard

for x in "banane" :
    print(x)

print("")#pour espacer tout ça, ça deviens illisible

result =0
for x in "Lait" :
    result +=1
print(result)
#technique avec une boucle

a = "lait"
print(len(a))
#avec len() c'est plus simple

#pour retrouver un mot dans une phrase :
phrase = "bonjour je suis une superbe phrase"
print("une" in phrase)   #si il y a "une" dans la phrase, ça renvoie True


if "phrase" in phrase :
    print("phrase")
#J'avoue c'est de la triche mais ça marche au moins

recherche = "phrase"
if recherche in phrase :
    print(recherche)
#voilà là c'est sexy



#le slicing

a="Exemple"
print(a[2:4]) #recupère du 2 au 4ème charactère (4 exclu)

#pour afficher que le m :
print(a[3])

#pour afficher pl en partant à l'envers
print(a[-3:-1])

print("")

print(bool("hello"), bool(15), bool(None), bool(0), bool(False), bool(""), bool(()), bool([]), bool({}))

#===========================================================

#Enfin les fonctions !!!

def MaFonction() :
    return True


print(MaFonction())

#Chalenge :
# voir si un nombre est entier
print("") #pour voir ce qu'on fait toujours

entier = 12
pasEntier = 25.2
autrePasEntier = "sisi je suis un entier t'inquiète"

def testEntier(valeur) :
    if type(valeur) == int :
        return True
    return False
#mais quelle fonction de qualitée !

def autreTestEntier(valeur):
    return isinstance(valeur, int)
#encore plus qualitative !

#pour tester la première :
print(testEntier(entier))
print(testEntier(pasEntier))
print(testEntier(autrePasEntier))

print("")

#pour tester la deuxième :
print(autreTestEntier(entier))
print(autreTestEntier(pasEntier))
print(autreTestEntier(autrePasEntier))

#nomenclature et présentation d'un code/projet

#Le tuples

# on ne peut pas modifier le tuple une fois créé

print("")

superTuple = (3,4,7)
print(type(superTuple))

b,c = (5,6)

print(b, "et",c)

def testTuple() :
    return 5,6

a = testTuple()
print(a, type(a))

for i in superTuple :
    print("dans mon tuple :",i)

print("")

superTuple = (8,)   #on peut faire un tuple avec un seul truc dedans
print(superTuple, "il est tout seul le pauvre")

print("")

#Récupérer l'élément unique présent dans le tuple

c = superTuple[0]
print(c, type(c))

u, = superTuple
print(u, type(u))

v, = c
print(v, type(c)) #ça marche pas parce que c est devenu un int

#Structuration d'un fichier



"""

PROGRAMME PRINCIPAL

Zone des imports
Zone déclaration des variables globales
Zone déclaration modules et fonctions
Le programme en soi
"""

